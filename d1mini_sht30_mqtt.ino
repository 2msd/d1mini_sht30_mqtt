/*
MWSN D1 WEMOS + SHT30 shield to MQTT
https://www.bakke.online/index.php/2017/05/21/reducing-wifi-power-consumption-on-esp8266-part-1/
https://www.bakke.online/index.php/2017/05/21/reducing-wifi-power-consumption-on-esp8266-part-2/
https://www.bakke.online/index.php/2017/05/22/reducing-wifi-power-consumption-on-esp8266-part-3/
https://www.bakke.online/index.php/2017/06/24/esp8266-wifi-power-reduction-avoiding-network-scan/
https://arduino-esp8266.readthedocs.io/en/latest/Troubleshooting/debugging.html
*/

#define VERSION "1.0"

#ifdef DEBUG_ESP_PORT
#define DEBUG_MSG(...) DEBUG_ESP_PORT.printf( __VA_ARGS__ )
#else
#define DEBUG_MSG(...)
#endif

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <ESP8266httpUpdate.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>
#include <FS.h>
#include <Wire.h>
#include <ClosedCube_SHT31D.h>

#include <math.h>


// ERROR CODES
#define ERROR_NONE           0
#define ERROR_NO_SENSOR      1
#define ERROR_NO_WIFI        2
#define ERROR_NO_CONF        3
#define ERROR_SIZE_CONF      4
#define ERROR_NO_ID          5
#define ERROR_NO_MQTT        6
#define ERROR_MQTT_SUB       7
#define ERROR_MQTT_PUB       8
#define ERROR_MQTT_NO_DATA  10
#define ERROR_OTA_NA        11
#define ERROR_OTA_FAIL      12

// Number of expected topics from MQTT
// MQTT_TOPIC_ROOT + /network/day day/night mode
// MQTT_TOPIC_ROOT + /network/ota over-the-air update
#define MQTT_TOPIC_ROOT "mwsn"
#define MQTT_TOPIC_NETWORK MQTT_TOPIC_ROOT "/network/#"
#define N_EXPECTED_TOPICS    2

// OTA address and file
#define OTA_IP    "1.2.3.4"
#define OTA_PORT  8888
#define OTA_PATH  "/d1mini.bin"

// Sleep short 3min
#define SHORT_SLEEP_S 180

// Sleep mid 15min
#define MID_SLEEP_S 900

// Sleep long 60min
#define LONG_SLEEP_S 3600

uint64_t sleep_time_s = SHORT_SLEEP_S;

// IP address of node is fixed + offset + id
#define IP_OFFSET 50

#define IP_A 192
#define IP_B 168
#define IP_C 1
#define IP_D 0

#define GW_A IP_A
#define GW_B IP_B
#define GW_C IP_C
#define GW_D 1

#define NM_A 255
#define NM_B 255
#define NM_C 255
#define NM_D 0

// Create abjects
WiFiClient wifi;
PubSubClient mqtt( wifi );
DynamicJsonBuffer jsonBuffer;
ClosedCube_SHT31D sht3xd;

bool res = false;
bool sent = false;
bool subscribed = false;
bool ota = false;
bool rtcValid = false;
uint8_t received = 0;

// The ESP8266 RTC memory is arranged into blocks of 4 bytes. The access methods read and write 4 bytes at a time,
// so the RTC data structure should be padded to a 4-byte multiple.
struct {
  uint32_t crc32;   // 4 bytes
  uint8_t channel;  // 1 byte,   5 in total
  uint8_t bssid[6]; // 6 bytes, 11 in total
  uint8_t last_error_code;  // 1 byte,  12 in total
  uint16_t fast_periods;  // 2 byte,  14 in total
  uint16_t mid_periods;  // 2 byte,  16 in total
  uint16_t slow_periods;  // 2 byte,  18 in total
  uint16_t padding;  // 2 byte,  20 in total
} rtcData;

struct {
  String id;
  String wifiSsid;
  String wifiPw;
  String mqttSvr;
  String mqttUsr;
  String mqttPw;
  String clientId;
  String mqttTopic;
} config;

void readConfig() {
  File configFile = SPIFFS.open( "/config.json", "r" );
  if ( !configFile ) {
    DEBUG_MSG( "No configuration, dying.\n" );
    goto_sleep(ERROR_NO_CONF);
    return;
  }

  if ( 256 < configFile.size() ) {
    DEBUG_MSG( "Config File size > 256! Dying.\n" );
    goto_sleep(ERROR_SIZE_CONF);
    return;
  }

  DEBUG_MSG("Reading config file\n");
  // setTimeout is needed otherwhise readString will take 1s!
  configFile.setTimeout( 0 );
  JsonObject& jsonConf = jsonBuffer.parseObject( configFile.readString() );
  configFile.close();

  config.id = jsonConf["id"].as<String>();
  DEBUG_MSG("config.id:%s\n", config.id.c_str());

  config.wifiSsid = jsonConf["wifi_ssid"].as<String>();
  DEBUG_MSG("config.wifiSsid:%s\n", config.wifiSsid.c_str());

  config.wifiPw = jsonConf["wifi_password"].as<String>();
  DEBUG_MSG("config.wifiPw:%s\n", config.wifiPw.c_str());

  config.mqttSvr = jsonConf["mqtt_server"].as<String>();
  DEBUG_MSG("config.mqttSvr:%s\n", config.mqttSvr.c_str());

  config.mqttUsr = jsonConf["mqtt_user"].as<String>();
  DEBUG_MSG("config.mqttUsr:%s\n", config.mqttUsr.c_str());

  config.mqttPw = jsonConf["mqtt_password"].as<String>();
  DEBUG_MSG("config.mqttPw:%s\n", config.mqttPw.c_str());

  config.clientId = String(MQTT_TOPIC_ROOT) + "-ESP8266-" + config.id;
  DEBUG_MSG("config.clientId:%s\n", config.clientId.c_str());

  config.mqttTopic = String(MQTT_TOPIC_ROOT) + "/sensors/" + config.id + "/data";  //Topic temperature
  DEBUG_MSG("config.mqttTopic:%s\n", config.mqttTopic.c_str());

  if ( 0 == config.id.toInt() ) {
    DEBUG_MSG( "Wrong ID, not a number > 0!\n" );
    goto_sleep(ERROR_NO_ID);
    return;
  }
}

uint32_t calculateCRC32( const uint8_t *data, size_t length ) {
  uint32_t crc = 0xffffffff;
  while( length-- ) {
    uint8_t c = *data++;
    for( uint32_t i = 0x80; i > 0; i >>= 1 ) {
      bool bit = crc & 0x80000000;
      if( c & i ) {
        bit = !bit;
      }

      crc <<= 1;
      if( bit ) {
        crc ^= 0x04c11db7;
      }
    }
  }

  return crc;
}

void checkRtcMem() {
  if( ESP.rtcUserMemoryRead( 0, (uint32_t*)&rtcData, sizeof( rtcData ) ) ) {
    // Calculate the CRC of what we just read from RTC memory, but skip the first 4 bytes as that's the checksum itself.
    uint32_t crc = calculateCRC32( ((uint8_t*)&rtcData) + 4, sizeof( rtcData ) - 4 );
    if( crc == rtcData.crc32 ) {
      DEBUG_MSG("Data in RTC valid: %x\n", rtcData.crc32);
      DEBUG_MSG("Channel: %x\n", rtcData.channel);
      DEBUG_MSG("BSSID: %x:%x:%x:%x:%x:%x\n", rtcData.bssid[0],rtcData.bssid[1],rtcData.bssid[2],rtcData.bssid[3],rtcData.bssid[4],rtcData.bssid[5]);
      rtcValid = true;
    } else {
      DEBUG_MSG("Data in RTC invalid\n");
      rtcData.last_error_code = 0;
      rtcData.fast_periods = 0;
      rtcData.mid_periods = 0;
      rtcData.slow_periods = 0;
    }
  }
}

void writeRtcMem(uint8_t error_code) {
  if ( error_code != ERROR_NONE ) {
    rtcData.last_error_code = error_code;
  }
  switch (sleep_time_s) {
    case LONG_SLEEP_S:
      rtcData.slow_periods++;
      break;
    case MID_SLEEP_S:
      rtcData.mid_periods++;
      break;
    default:
      rtcData.fast_periods++;
      break;
  }
  rtcData.crc32 = calculateCRC32( ((uint8_t*)&rtcData) + 4, sizeof( rtcData ) - 4 );
  ESP.rtcUserMemoryWrite( 0, (uint32_t*)&rtcData, sizeof( rtcData ) );
}

void turnOnWifi() {
  // Bring up the WiFi connection
  DEBUG_MSG("Starting WiFi\n");
  IPAddress ip( IP_A, IP_B, IP_C, IP_OFFSET + config.id.toInt() );
  IPAddress gateway( GW_A, GW_B, GW_C, GW_D );
  IPAddress subnet( NM_A, NM_B, NM_C, NM_D );
  WiFi.forceSleepWake();
  delay( 1 );
  WiFi.persistent( false );
  WiFi.mode( WIFI_STA );
  WiFi.config( ip, gateway, subnet );

  if( rtcValid ) {
    // The RTC data was good, make a quick connection
    WiFi.begin( config.wifiSsid.c_str(), config.wifiPw.c_str(), rtcData.channel, rtcData.bssid, true );
  } else {
    // The RTC data was not valid, so make a regular connection
    WiFi.begin( config.wifiSsid.c_str(), config.wifiPw.c_str() );
  }
}

void checkWifi() {
  int retries = 0;
  int wifiStatus = WiFi.status();
  while( wifiStatus != WL_CONNECTED ) {
    retries++;
    if( retries == 100 ) {
      DEBUG_MSG( "Quick connect is not working, reset WiFi and try regular connection!\n" );
      // Quick connect is not working, reset WiFi and try regular connection
      WiFi.disconnect();
      delay( 10 );
      WiFi.forceSleepBegin();
      delay( 10 );
      WiFi.forceSleepWake();
      delay( 10 );
      WiFi.begin( config.wifiSsid.c_str(), config.wifiPw.c_str() );
    }
    if( retries == 200 ) {
      // Giving up after 10 seconds and going back to sleep
      DEBUG_MSG( "Could not connect to WiFi!\n" );
      goto_sleep(ERROR_NO_WIFI);
      return; // Not expecting this to be called, the previous call will never return.
    }
    delay( 50 );
    wifiStatus = WiFi.status();
  }
  DEBUG_MSG("WiFi Connected, r:%d\n", retries);
  // Cache WiFi information
  rtcData.channel = WiFi.channel();
  memcpy( rtcData.bssid, WiFi.BSSID(), 6 ); // Copy 6 bytes of BSSID (AP's MAC address)
}

void runMQTT(const char* dataStr){
  DEBUG_MSG("Connect to MQTT server\n");
  mqtt.setServer( config.mqttSvr.c_str(), 1883 );    // Configure MQTT connecion
  mqtt.setCallback( callback );           // callback function to execute when a MQTT message
  res = mqtt.connect( config.clientId.c_str(), config.mqttUsr.c_str(), config.mqttPw.c_str() );
  if ( ! res ) {
    DEBUG_MSG( "Connect failed\n" );
    goto_sleep(ERROR_NO_MQTT);
  }
  sent = mqtt.publish( config.mqttTopic.c_str(), dataStr, true );   // Publish data on topic
  if ( ! sent ) {
    DEBUG_MSG( "Publish failed\n" );
    goto_sleep(ERROR_MQTT_PUB);
  }
  subscribed = mqtt.subscribe(MQTT_TOPIC_NETWORK);
  if ( ! subscribed ) {
    DEBUG_MSG( "Subscribe failed\n" );
    goto_sleep(ERROR_MQTT_SUB);
  }

  int retries = 0;
  mqtt.loop();
  while ( N_EXPECTED_TOPICS > received ) {
    retries++;
    if( retries == 100 ) {
      goto_sleep(ERROR_MQTT_NO_DATA);
    }
    delay ( 10 );
    mqtt.loop();
  }
  if (ota) {
    t_httpUpdate_return ret = ESPhttpUpdate.update(OTA_IP, OTA_PORT, OTA_PATH);
    switch(ret) {
      case HTTP_UPDATE_FAILED:
        DEBUG_MSG("[update] Update failed.\n");
        goto_sleep(ERROR_OTA_FAIL);
        break;
      case HTTP_UPDATE_NO_UPDATES:
        DEBUG_MSG("[update] Update no Update.\n");
        goto_sleep(ERROR_OTA_NA);
        break;
      case HTTP_UPDATE_OK:
        DEBUG_MSG("[update] Update ok.\n"); // may not called we reboot the ESP
        break;
    }
  }
  // mqtt.disconnect();
}

void callback( char* topic, byte* payload, unsigned int length ) {
  char c = (char)payload[0];
  DEBUG_MSG( "Message arrived [" );
  DEBUG_MSG( topic );
  DEBUG_MSG( "] %c\n", c );
  String thisTopic = String(topic);
  if (thisTopic == String(MQTT_TOPIC_ROOT) + "/network/day") {
    if ( '0' == c ) {
      DEBUG_MSG( "Night mode\n" );
      sleep_time_s = LONG_SLEEP_S;
    }
    if ( '1' == c ) {
      DEBUG_MSG( "Evening mode\n" );
      sleep_time_s = MID_SLEEP_S;
    }
    if ( '2' == c ) {
      DEBUG_MSG( "Day mode\n" );
      sleep_time_s = SHORT_SLEEP_S;
    }
  }
  if (thisTopic == String(MQTT_TOPIC_ROOT) + "/network/ota") {
    String newVersion = String((char*)payload);
    String thisVersion = String(VERSION);
    if ( newVersion != thisVersion ) {
      DEBUG_MSG( "OTA mode, update from %s to %s\n", thisVersion.c_str(), newVersion.c_str() );
      ota = true;
    }
  }
  received++;
}

void goto_sleep(uint8_t error_code) {
  DEBUG_MSG( "Error code: %d\n", error_code );
  writeRtcMem(error_code);
  WiFi.disconnect( true );
  WiFi.mode( WIFI_OFF );
  WiFi.forceSleepBegin();
  delay( 1 );
  DEBUG_MSG("Go to sleep for %d seconds\n", sleep_time_s);
  DEBUG_MSG("SYSTEM TIME tEnd %lu millseconds\n", millis());
  ESP.deepSleep( sleep_time_s * 1e6, WAKE_RF_DISABLED );
}

void setup() {
  String outputData;
  int retries = 0;
  float t, h, v;
  WiFi.mode( WIFI_OFF );
  WiFi.forceSleepBegin();
  delay( 1 );
  SPIFFS.begin();
  Wire.begin();
  sht3xd.begin(0x45); // I2C address: 0x44 or 0x45

#ifdef DEBUG_ESP_PORT
  Serial.begin( 115200 );
  Serial.setTimeout( 2000 );
  // Wait for serial to initialize.
  while( !Serial ) { }
#endif

  DEBUG_MSG("Start SHT30+DeepSleep+MQTT+DayNight+OTA\n");
  DEBUG_MSG("Version %s\n", VERSION);
  DEBUG_MSG("SYSTEM TIME t0 %lu millseconds\n", millis());

  readConfig();

  checkRtcMem();
  DEBUG_MSG("SYSTEM TIME t1 %lu millseconds\n", millis());

  turnOnWifi();
  DEBUG_MSG("SYSTEM TIME t2 %lu millseconds\n", millis());

  // Read Sensors
  SHT31D sensor = sht3xd.readTempAndHumidity(SHT3XD_REPEATABILITY_LOW, SHT3XD_MODE_CLOCK_STRETCH, 50);
  if (sensor.error == SHT3XD_NO_ERROR) {
    t = round ( sensor.t * 10.0f ) / 10.0f;
    h = round ( sensor.rh * 10.0f ) / 10.0f;
  } else {
    DEBUG_MSG( "No readings from SHT30 sensor %d!\n", sensor.error );
    goto_sleep(ERROR_NO_SENSOR);
  }

  DEBUG_MSG("SYSTEM TIME t3 %lu millseconds\n", millis());
  // Read ADC
  pinMode(A0, INPUT);
  v = analogRead(A0) / 1023.0f;
  v = round ( v * 3200.0f ) / 1000.0f;

  DEBUG_MSG("t:%f\n",t);
  DEBUG_MSG("h:%f\n",h);
  DEBUG_MSG("v:%f\n", v);

  JsonObject& data = jsonBuffer.createObject();
  data["t"] = t;
  data["h"] = h;
  data["v"] = v;
  // data["ft"] = ft;
  data["ec"] = rtcData.last_error_code;
  data["fp"] = rtcData.fast_periods;
  data["mp"] = rtcData.mid_periods;
  data["sp"] = rtcData.slow_periods;
  data["ve"] = VERSION;
  data.printTo(outputData);
  DEBUG_MSG("%s\n", outputData.c_str());

  checkWifi();
  DEBUG_MSG("SYSTEM TIME t4 %lu millseconds\n", millis());
  runMQTT(outputData.c_str());
  DEBUG_MSG("SYSTEM TIME t5 %lu millseconds\n", millis());

  goto_sleep(ERROR_NONE);
}

void loop() {
  // Never reached!
}
